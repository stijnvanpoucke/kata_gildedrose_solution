package com.gildedrose.endofdaystrategy.implementation;

import com.gildedrose.endofdaystrategy.EndOfDayStrategy;
import com.gildedrose.endofdaystrategy.implementations.DefaultEndOfDayStrategy;
import com.gildedrose.item.ItemProxy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("DefaultEndOfDay on endOfDayUpdate()")
class DefaultEndOfDayStrategyTest {

    @Mock
    private ItemProxy itemProxy;

    private EndOfDayStrategy strategy = new DefaultEndOfDayStrategy();

    @Test
    @DisplayName("should decrease the quality by 1 when expiration has not yet been reached")
    void decreaseBy2() {
        //Given
        doReturn(false).when(itemProxy).isExpired();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).decreaseQuality(1);
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }

    @Test
    @DisplayName("should decrease the quality by 2 when expiration has been reached")
    void decreaseBy4() {
        //Given
        doReturn(true).when(itemProxy).isExpired();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).decreaseQuality(2);
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }
}
