package com.gildedrose.endofdaystrategy.implementation;

import com.gildedrose.endofdaystrategy.EndOfDayStrategy;
import com.gildedrose.endofdaystrategy.implementations.SulfurasEndOfDayStrategy;
import com.gildedrose.item.ItemProxy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Sulfuras on endOfDayUpdate()")
class SulfurasEndOfDayStrategyTest {

    @Mock
    private ItemProxy itemProxy;

    private EndOfDayStrategy strategy = new SulfurasEndOfDayStrategy();

    @Test
    @DisplayName("should do nothing")
    void doNothing() {
        //Given

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy, never()).decreaseQuality(anyInt());
        verify(itemProxy, never()).increaseQuality(anyInt());
        verify(itemProxy, never()).decreaseAmountOfDaysUntilExpiration();
    }
}