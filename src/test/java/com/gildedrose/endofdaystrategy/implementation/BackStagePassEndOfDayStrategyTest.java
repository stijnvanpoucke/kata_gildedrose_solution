package com.gildedrose.endofdaystrategy.implementation;

import com.gildedrose.endofdaystrategy.EndOfDayStrategy;
import com.gildedrose.endofdaystrategy.implementations.BackStagePassEndOfDayStrategy;
import com.gildedrose.item.ItemProxy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("BackStagePassEndOfDayStrategy on endOfDayUpdate()")
class BackStagePassEndOfDayStrategyTest {

    @Mock
    private ItemProxy itemProxy;

    private EndOfDayStrategy strategy = new BackStagePassEndOfDayStrategy();

    @Test
    @DisplayName("should set the quality to zero when the expiration has been reached")
    void qualityToZeroWhenExpired() {
        //Given
        doReturn(true).when(itemProxy).isExpired();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).setQualityToZero();
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }

    @Test
    @DisplayName("should increase the quality with 2 when the days until expiration is between 5 and 10")
    void qualityIncreaseBy2IfBetween5and10Days() {
        //Given
        doReturn(false).when(itemProxy).isExpired();
        doReturn(true).when(itemProxy).expirationBetween5and10days();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).increaseQuality(2);
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }

    @Test
    @DisplayName("should increase the quality with 3 when the days until expiration is between 0 and 5")
    void qualityIncreaseBy3If5Days() {
        //Given
        doReturn(false).when(itemProxy).isExpired();
        doReturn(true).when(itemProxy).expirationInLessThen5Days();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).increaseQuality(3);
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }

    @Test
    @DisplayName("should increase the quality with 1 when the days until expiration is over 10")
    void qualityIncreaseBy1IfMoreThen10() {
        //Given
        doReturn(false).when(itemProxy).isExpired();
        doReturn(false).when(itemProxy).expirationInLessThen5Days();
        doReturn(false).when(itemProxy).expirationBetween5and10days();

        //When
        strategy.endOfDayUpdate(itemProxy);

        //Then
        verify(itemProxy).increaseQuality(1);
        verify(itemProxy).decreaseAmountOfDaysUntilExpiration();
    }
}