package com.gildedrose.endofdaystrategy;

import com.gildedrose.endofdaystrategy.implementations.*;
import com.gildedrose.item.ItemProxy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EndOfDayStrategyFactoryTest {

    private static final String SULFURAS = "sulfuras";
    private static final String BRIE = "brie";
    private static final String BACKSTAGE_PASS = "backstage pass";
    private static final String CONJURED = "conjured";

    @Mock
    private ItemProxy itemProxy;

    @Test
    @DisplayName("Return the sulfuras end of day strategy when the item name contains the word sulfuras")
    void returnSulfurasStrategy() {
        //Given
        when(itemProxy.containsName(SULFURAS)).thenReturn(true);

        //When
        EndOfDayStrategy actual = EndOfDayStrategyFactory.createStrategy(itemProxy);

        //Then
        assertTrue(actual instanceof SulfurasEndOfDayStrategy);
    }

    @Test
    @DisplayName("Return the Brie end of day strategy when the item name contains the word brie")
    void returnBrieStrategy() {
        //Given
        doReturn(false).when(itemProxy).containsName(SULFURAS);
        doReturn(true).when(itemProxy).containsName(BRIE);

        //When
        EndOfDayStrategy actual = EndOfDayStrategyFactory.createStrategy(itemProxy);

        //Then
        assertTrue(actual instanceof BrieEndOfDayStrategy);
    }

    @Test
    @DisplayName("Return the backstage pass end of day strategy when the item name contains the word backstage pass")
    void returnBackstagePassStrategy() {
        //Given
        doReturn(false).when(itemProxy).containsName(SULFURAS);
        doReturn(false).when(itemProxy).containsName(BRIE);
        doReturn(true).when(itemProxy).containsName(BACKSTAGE_PASS);

        //When
        EndOfDayStrategy actual = EndOfDayStrategyFactory.createStrategy(itemProxy);

        //Then
        assertTrue(actual instanceof BackStagePassEndOfDayStrategy);
    }

    @Test
    @DisplayName("Return the Conjure end of day strategy when the item name contains the word Conjure")
    void returnConjureStrategy() {
        //Given
        doReturn(false).when(itemProxy).containsName(SULFURAS);
        doReturn(false).when(itemProxy).containsName(BRIE);
        doReturn(false).when(itemProxy).containsName(BACKSTAGE_PASS);
        doReturn(true).when(itemProxy).containsName(CONJURED);

        //When
        EndOfDayStrategy actual = EndOfDayStrategyFactory.createStrategy(itemProxy);

        //Then
        assertTrue(actual instanceof ConjuredEndOfDayStrategy);
    }

    @Test
    @DisplayName("Return the default end of day strategy when the item name doesn't match any other strategy")
    void returnDefaultStrategy() {
        //Given
        doReturn(false).when(itemProxy).containsName(SULFURAS);
        doReturn(false).when(itemProxy).containsName(BRIE);
        doReturn(false).when(itemProxy).containsName(BACKSTAGE_PASS);
        doReturn(false).when(itemProxy).containsName(CONJURED);

        //When
        EndOfDayStrategy actual = EndOfDayStrategyFactory.createStrategy(itemProxy);

        //Then
        assertTrue(actual instanceof DefaultEndOfDayStrategy);
    }
}