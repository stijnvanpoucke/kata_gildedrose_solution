package com.gildedrose.item;

import com.gildedrose.Item;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("ItemProxyImpl should delegate to Item and...")
class ItemProxyImplTest {

    private static final String TEST_NAME = "fakeTestName";
    private static final int TEST_SELL_IN = 10;
    private static final int TEST_QUALITY = 10;

    @Test
    @DisplayName("quality getter delegates to item")
    void getQuality() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY));

        //When
        int actual = proxy.getQuality();

        //Then
        assertEquals(actual, TEST_QUALITY);
    }

    @Test
    @DisplayName("qame getter delegates to item")
    void getName() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY));

        //When
        String actual = proxy.getName();

        //Then
        assertEquals(actual, TEST_NAME);
    }

    @Test
    @DisplayName("days Until Expiration getter delegates to SellIn on item")
    void getDaysUntilExpiration() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY));

        //When
        int actual = proxy.getDaysUntilExpiration();

        //Then
        assertEquals(actual, TEST_SELL_IN);
    }

    @Test
    @DisplayName("expired should return false when SellIn is more then 0")
    void isExpiredWhenSellInMoreThen0() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, 1, TEST_QUALITY));

        //When
        boolean actual = proxy.isExpired();

        //Then
        assertFalse(actual);
    }

    @Test
    @DisplayName("Expired should return true when SellIn is 0")
    void isExpiredWhenSellIs0() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, 0, TEST_QUALITY));

        //When
        boolean actual = proxy.isExpired();

        //Then
        assertTrue(actual);
    }

    @Test
    @DisplayName("expired should return true when SellIn is negative")
    void isExpiredWhenSellInLessThen0() {
        //Given
        ItemProxy proxy = new ItemProxyImpl(new Item(TEST_NAME, -1, TEST_QUALITY));

        //When
        boolean actual = proxy.isExpired();

        //Then
        assertTrue(actual);
    }

    @Test
    @DisplayName("increase quality should delegate to item")
    void increaseQuality() {
        //Given
        Item item = new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.increaseQuality(5);

        //Then
        assertEquals(TEST_QUALITY + 5, item.quality);
        assertEquals(TEST_QUALITY + 5, proxy.getQuality());
    }

    @Test
    @DisplayName("decrease quality should delegate to item")
    void decreaseQuality() {
        //Given
        Item item = new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.decreaseQuality(5);

        //Then
        assertEquals(TEST_QUALITY - 5, item.quality);
        assertEquals(TEST_QUALITY - 5, proxy.getQuality());
    }

    @Test
    @DisplayName("set quality to zero should delegate to item and set it to 0")
    void setQualityToZero() {
        //Given
        Item item = new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.setQualityToZero();

        //Then
        assertEquals(0, item.quality);
        assertEquals(0, proxy.getQuality());
    }

    @Test
    @DisplayName("decrease amount of days until expiration should subtract 1 from the item sellIn")
    void decreaseAmountOfDaysUntilExpiration() {
        //Given
        Item item = new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.decreaseAmountOfDaysUntilExpiration();

        //Then
        assertEquals(TEST_SELL_IN -1, item.sellIn);
        assertEquals(TEST_SELL_IN - 1, proxy.getDaysUntilExpiration());
    }

    @Test
    @DisplayName("set maximum quality should set the quality to 50 on the delegated item")
    void setMaximumQuality() {
        //Given
        Item item = new Item(TEST_NAME, TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.setMaximumQuality();

        //Then
        assertEquals(50, item.quality);
        assertEquals(50, proxy.getQuality());
    }

    @Test
    @DisplayName("containsName should return true when a non case sensitive match has been found")
    void containsNameWhenItDoes() {
        //Given
        Item item = new Item("TESt_me", TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.containsName("test");

        //Then
        assertTrue(actual);
    }

    @Test
    @DisplayName("containsName should return false when no non case sensitive match has been found")
    void containsNameWhenItDoesnt() {
        //Given
        Item item = new Item("vader", TEST_SELL_IN, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.containsName("skywalker");

        //Then
        assertFalse(actual);
    }


    @Test
    @DisplayName("quality can never be set to a higher value then 50, when it's on 50")
    void neverInCreaseQualityAbove50When50() {
        //Given
        Item item = new Item("vader", TEST_SELL_IN, 50);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.increaseQuality(2);

        //Then
        assertEquals(50, item.quality);
        assertEquals(50, proxy.getQuality());
    }

    @Test
    @DisplayName("quality can never be set to a higher value then 50, when it's currently below 50")
    void neverInCreaseQualityAbove50() {
        //Given
        Item item = new Item("vader", TEST_SELL_IN, 49);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.increaseQuality(2);

        //Then
        assertEquals(50, item.quality);
        assertEquals(50, proxy.getQuality());
    }

    @Test
    @DisplayName("quality can never be set to a lower value then 0, when it's on 0")
    void neverDecreaseQualityBelow0hen0() {
        //Given
        Item item = new Item("vader", TEST_SELL_IN, 0);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.decreaseQuality(1);

        //Then
        assertEquals(0, item.quality);
        assertEquals(0, proxy.getQuality());
    }

    @Test
    @DisplayName("quality can never be set to a lower value then 0")
    void neverInDecreaseQualityBelow0() {
        //Given
        Item item = new Item("vader", TEST_SELL_IN, 2);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        proxy.decreaseQuality(3);

        //Then
        assertEquals(0, item.quality);
        assertEquals(0, proxy.getQuality());
    }

    @Test
    void expirationBetween10Or5DaysUpperBound() {
        //Given
        Item item = new Item("vader", 10, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationBetween5and10days();

        //Then
        assertTrue(actual);
    }

    @Test
    void expirationBetween10Or5DaysAboveUpperBound() {
        //Given
        Item item = new Item("vader", 11, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationBetween5and10days();

        //Then
        assertFalse(actual);
    }

    @Test
    void expirationBetween10Or5DaysLowerBound() {
        //Given
        Item item = new Item("vader", 6, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationBetween5and10days();

        //Then
        assertTrue(actual);
    }


    @Test
    void expirationBetween10Or5DaysBelowLowerBound() {
        //Given
        Item item = new Item("vader", 5, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationBetween5and10days();

        //Then
        assertFalse(actual);
    }

    @Test
    void expirationBetween5DaysOrLessUpperBound() {
        //Given
        Item item = new Item("vader", 5, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationInLessThen5Days();

        //Then
        assertTrue(actual);
    }

    @Test
    void expirationBetween5DaysOrLessAboveUpperBound() {
        //Given
        Item item = new Item("vader", 6, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationInLessThen5Days();

        //Then
        assertFalse(actual);
    }

    @Test
    void expirationBetween5DaysOrLessLowerBound() {
        //Given
        Item item = new Item("vader", 1, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationInLessThen5Days();

        //Then
        assertTrue(actual);
    }

    @Test
    void expirationBetween5DaysOrLessBelowBound() {
        //Given
        Item item = new Item("vader", 0, TEST_QUALITY);
        ItemProxy proxy = new ItemProxyImpl(item);

        //When
        boolean actual = proxy.expirationInLessThen5Days();

        //Then
        assertFalse(actual);
    }
}