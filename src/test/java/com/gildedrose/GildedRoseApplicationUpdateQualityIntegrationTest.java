package com.gildedrose;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayName(("Application End Of Day Quality Update Integration Tests"))
class GildedRoseApplicationUpdateQualityIntegrationTest {

    private static final String A_ITEM_NAME_FOR_CLUB_SANDWISH = "Club Sandwish";
    private static final String A_ITEM_NAME_FOR_AGED_BRIE = "Aged Brie";
    private static final String A_ITEM_NAME_FOR_SULFURAS = "Sulfuras, Hand of Ragnaros";
    private static final String A_ITEM_NAME_FOR_BACKSTAGE_PASS = "Backstage passes to a TAFKAL80ETC concert";
    private static final String A_ITEM_NAME_FOR__CONJURED = "Conjured Man Cake";

    @Nested
    @DisplayName("Items without exceptional cases should behave like this")
    class regularItem {

        @Test
        @DisplayName("At the end of the day the number of remaining days to sell before expiration and the quality should be decrease by 1")
        void endOfTheDayDecrease() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_CLUB_SANDWISH, 10, 50) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    () -> assertEquals(A_ITEM_NAME_FOR_CLUB_SANDWISH, app.getFirstItem().getName()),
                    () -> assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    () -> assertEquals(49, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("No exceptional behavior on expiration day, decrease quality by 1")
        void expirationDay() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_CLUB_SANDWISH, 1, 50) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    () -> assertEquals(A_ITEM_NAME_FOR_CLUB_SANDWISH, app.getFirstItem().getName()),
                    () -> assertEquals(0, app.getFirstItem().getDaysUntilExpiration()),
                    () -> assertEquals(49, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("At the end of the day the quality should decreased by 2 when the item is expired")
        void doubleDecreaseQuality() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_CLUB_SANDWISH, 0, 50) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    () -> assertEquals(A_ITEM_NAME_FOR_CLUB_SANDWISH, app.getFirstItem().getName()),
                    () -> assertEquals(-1, app.getFirstItem().getDaysUntilExpiration()),
                    () -> assertEquals(48, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should never decrease to a negative value")
        void avoidDecreaseToNegativeValue() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_CLUB_SANDWISH, 0, 0) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_CLUB_SANDWISH, app.getFirstItem().getName()),
                    ()->assertEquals(-1, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(0, app.getFirstItem().getQuality()));
        }
    }

    @Nested
    @DisplayName("The quality of Aged Brie increases by time ")
    class AgedBrie {

        @Test
        @DisplayName("The quality of aged brie should increase by 1 at the end of the day")
        void qualityOfAgedBrieIncreases() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_AGED_BRIE, 10, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_AGED_BRIE, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(11, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality of aged brie should increase by 1 even it it's 0")
        void qualityOfAgedBrieIncreasesOnZero() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_AGED_BRIE, 10, 0) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_AGED_BRIE, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(1, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality of aged brie should increase by 1 even it it's less then 0")
        void qualityOfAgedBrieIncreasesLessThenZero() {
            //Given
            Item[] items = new Item[] { new Item("Aged Brie", 10, -10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_AGED_BRIE, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(-9, app.getFirstItem().getQuality()));
        }

        //Remarkable because the increase by 2 was not exactly specified in the requirements
        @Test
        @DisplayName("The quality of aged brie should after the expiration increase by 2")
        void qualityOfAgeExpiredBrieIncreases() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_AGED_BRIE, 0, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_AGED_BRIE, app.getFirstItem().getName()),
                    ()->assertEquals(-1, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(12, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality can never be above 50")
        void dontIncreaseAbove50() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_AGED_BRIE, 10, 50) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_AGED_BRIE, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(50, app.getFirstItem().getQuality()));
        }
    }

    @Nested
    @DisplayName("Sulfuras is a legendary item, it never has to be sold or decreases in Quality")
    class Sulfaris {

        @Test
        @DisplayName("The days until expiration doesn't change and the quality stays on 80")
        void stayConsistentAtTheEndOfTheDay() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_SULFURAS, 100, 80) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_SULFURAS, app.getFirstItem().getName()),
                    ()->assertEquals(100, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(80, app.getFirstItem().getQuality()));
        }

        //Remarkable, to be suggested for improvement
        @Test
        @DisplayName("Sulfuras can be inputted incorrectly with a negative number of days until expiration and quality different then 80")
        void canBeInputtedIncorrectly() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_SULFURAS, -10, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_SULFURAS, app.getFirstItem().getName()),
                    ()->assertEquals(-10, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(10, app.getFirstItem().getQuality()));
        }
    }

    @Nested
    @DisplayName("Backstage passes are increasing in value once the expire date approaches")
    class BackStagePass {

        @Test
        @DisplayName("The quality should increase by 1 when there are still more then 10 days to go")
        void increaseBy1WhenSellInMoreThen10Days() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 12, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(11, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(11, app.getFirstItem().getQuality()));
        }

        //Remarkable that the quality is calculated before decreasing the sellin days
        @Test
        @DisplayName("The quality should increase by 1 when the number of days until expiration becomes exactly 10 days")
        void sellIn10Days() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 11, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(10, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(11, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should increase by 2 when the number of days until expiration before the calculation is less then 10 days and more then 5 days")
        void increaseBy2WhenSellInMoreLessThen10Morethen5() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 10, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(12, app.getFirstItem().getQuality()));
        }

        //Remarkable that the quality is calculated before decreasing the sellin days
        @Test
        @DisplayName("The quality should increase by 2 when the days until expiration becomes exactly 5 days")
        void sellIn5() {
            //Given
            Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 6, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(5, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(12, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should increase by 3 when the number of days until expiration before the calculation is less then 5 days")
        void increaseBy3WhenSellInMoreLessThen5() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 5, 10) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(4, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(13, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should never increase above 50")
        void above50() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 12, 50) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(11, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(50, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should never increase above 50, even when increased by 2")
        void above50Between10And5() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR_BACKSTAGE_PASS, 10, 49) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(50, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should never increase above 50, even when increased by 3")
        void above50Between5And0() {
            //Given
            Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 48) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(4, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(50, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("The quality should drop to zero when the item has expired (after calculation)")
        void quality0AfterExpiry() {
            //Given
            Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 45) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR_BACKSTAGE_PASS, app.getFirstItem().getName()),
                    ()->assertEquals(-1, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(0, app.getFirstItem().getQuality()));
        }
    }

    @Nested
    @DisplayName("Conjured items degrade twice as gast")
    class Conjured {

        @Test
        @DisplayName("Should decrease 2 points in quality")
        void degradeTwice() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR__CONJURED, 10, 40) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals("Conjured Man Cake", app.getFirstItem().getName()),
                    ()->assertEquals(9, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(38, app.getFirstItem().getQuality()));
        }

        @Test
        @DisplayName("Should decrease 4 points in quality when expired")
        void degradeFour() {
            //Given
            Item[] items = new Item[] { new Item(A_ITEM_NAME_FOR__CONJURED, 0, 40) };
            GildedRoseApplication app = new GildedRoseApplication(items);

            //When
            app.updateQuality();

            //Then
            assertAll("item",
                    ()->assertEquals(A_ITEM_NAME_FOR__CONJURED, app.getFirstItem().getName()),
                    ()->assertEquals(-1, app.getFirstItem().getDaysUntilExpiration()),
                    ()->assertEquals(36, app.getFirstItem().getQuality()));
        }

    }
}