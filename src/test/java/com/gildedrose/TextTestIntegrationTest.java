package com.gildedrose;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("TextTest Integration Test")
class TextTestIntegrationTest {

    private static String NEWLINE = System.getProperty("line.separator");

    @Test
    @DisplayName("Output matches the initial output which was saved to plain text before the refactoring")
    void textTestIntegrationTest() throws IOException {
        //Given
        String expected = new String(Files.readAllBytes(Paths.get("TextTest-Golden-Master.txt")));

        //When
        String actual = runTestAndGenerateTextTestResult(10);

        //Then
        assertEquals(expected, actual,"The TextTest Golder Master generated before the refactoring doesn't match the actual result");
    }

    private String runTestAndGenerateTextTestResult(int days) {
        StringBuilder output = new StringBuilder();

        output.append("OMGHAI!");

        Item[] items = new Item[] {
                new Item("+5 Dexterity Vest", 10, 20), //
                new Item("Aged Brie", 2, 0), //
                new Item("Elixir of the Mongoose", 5, 7), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                // this conjured item does not work properly yet
                new Item("Conjured Mana Cake", 3, 20) };

        GildedRoseApplication app = new GildedRoseApplication(items);

        for (int i = 0; i <= days; i++) {
            output.append(NEWLINE).append("-------- day ").append(i).append(" --------");
            output.append(NEWLINE).append("name, sellIn, quality");
            for (Item item : items) {
                output.append(NEWLINE).append(item);
            }
            output.append(NEWLINE);
            app.updateQuality();
        }

        return output.toString();
    }

}
