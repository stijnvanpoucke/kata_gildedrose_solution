package com.gildedrose.item;

public interface ItemProxy {

    boolean isExpired();

    int getQuality();

    String getName();

    int getDaysUntilExpiration();

    void increaseQuality(int amount);

    void decreaseQuality(int amount);

    void setQualityToZero();

    void decreaseAmountOfDaysUntilExpiration();

    void setMaximumQuality();

    boolean containsName(String name);

    boolean expirationBetween5and10days();

    boolean expirationInLessThen5Days();
}
