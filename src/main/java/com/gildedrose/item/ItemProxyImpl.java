package com.gildedrose.item;

import com.gildedrose.Item;

//Makes the code independent of the Item class which is out of our control + provides extended features
public class ItemProxyImpl implements ItemProxy {

    private static final int MAXIMUM_QUALITY_VALUE = 50;

    private Item item;

    public ItemProxyImpl(Item item) {
        this.item = item;
    }

    public boolean isExpired() {
        return this.getDaysUntilExpiration() < 1;
    }

    public int getQuality() {
        return item.quality;
    }

    public String getName() {
        return item.name;
    }

    public int getDaysUntilExpiration() {
        return item.sellIn;
    }

    public void increaseQuality(int amount) {
        if (this.item.quality+amount <= MAXIMUM_QUALITY_VALUE) {
            this.item.quality+=amount;
        } else {
            this.setMaximumQuality();
        }
    }

    public void decreaseQuality(int amount) {
        if (this.item.quality-amount >= 0) {
            this.item.quality-=amount;
        } else {
            this.item.quality = 0;
        }
    }

    public void setQualityToZero() {
        this.item.quality = 0;
    }

    public void decreaseAmountOfDaysUntilExpiration() {
        this.item.sellIn--;
    }

    @Override
    public void setMaximumQuality() {
        this.item.quality = MAXIMUM_QUALITY_VALUE;
    }

    public boolean containsName(String name) {
        return this.getName().toLowerCase().contains(name);
    }

    @Override
    public boolean expirationBetween5and10days() {
        return this.getDaysUntilExpiration() <= 10 && this.getDaysUntilExpiration() > 5;
    }

    @Override
    public boolean expirationInLessThen5Days() {
        return this.getDaysUntilExpiration() <= 5 && this.getDaysUntilExpiration() > 0;
    }
}