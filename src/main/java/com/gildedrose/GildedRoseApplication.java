package com.gildedrose;

import com.gildedrose.item.ItemProxy;
import com.gildedrose.item.ItemProxyImpl;

import java.util.List;

import static com.gildedrose.endofdaystrategy.EndOfDayStrategyFactory.createStrategy;
import static java.util.Arrays.*;
import static java.util.stream.Collectors.toList;

class GildedRoseApplication {

    private List<ItemProxy> items;

    public GildedRoseApplication(Item[] items) {
        this.items = stream(items).map(ItemProxyImpl::new).collect(toList());
    }

    public void updateQuality() {
        items.forEach(item -> createStrategy(item).endOfDayUpdate(item));
    }

    ItemProxy getFirstItem() {
        return this.items.get(0);
    }
}