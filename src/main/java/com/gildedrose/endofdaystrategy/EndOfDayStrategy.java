package com.gildedrose.endofdaystrategy;

import com.gildedrose.item.ItemProxy;

public interface EndOfDayStrategy {

   void endOfDayUpdate(ItemProxy item);

}
