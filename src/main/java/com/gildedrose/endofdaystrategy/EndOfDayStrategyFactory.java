package com.gildedrose.endofdaystrategy;

import com.gildedrose.endofdaystrategy.implementations.*;
import com.gildedrose.item.ItemProxy;

public class EndOfDayStrategyFactory {

    private static final String SULFURAS = "sulfuras";
    private static final String BRIE = "brie";
    private static final String BACKSTAGE_PASS = "backstage pass";
    private static final String CONJURED = "conjured";

    public static EndOfDayStrategy createStrategy(ItemProxy item) {
        if (item.containsName(SULFURAS)) {
            return new SulfurasEndOfDayStrategy();
        }
        else if (item.containsName(BRIE)) {
            return new BrieEndOfDayStrategy();
        }
        else if (item.containsName(BACKSTAGE_PASS)) {
            return new BackStagePassEndOfDayStrategy();
        }
        else if (item.containsName(CONJURED)) {
            return new ConjuredEndOfDayStrategy();
        }
        else {
            return new DefaultEndOfDayStrategy();
        }
    }
}