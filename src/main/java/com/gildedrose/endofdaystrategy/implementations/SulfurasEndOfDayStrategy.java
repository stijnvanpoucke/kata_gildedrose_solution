package com.gildedrose.endofdaystrategy.implementations;

import com.gildedrose.endofdaystrategy.EndOfDayStrategy;
import com.gildedrose.item.ItemProxy;

public class SulfurasEndOfDayStrategy implements EndOfDayStrategy {

    @Override
    public void endOfDayUpdate(ItemProxy item) {
        //do nothing for sulfuras
    }
}
