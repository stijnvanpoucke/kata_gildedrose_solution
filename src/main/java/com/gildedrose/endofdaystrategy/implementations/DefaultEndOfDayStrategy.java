package com.gildedrose.endofdaystrategy.implementations;

import com.gildedrose.endofdaystrategy.EndOfDayStrategy;
import com.gildedrose.item.ItemProxy;

public class DefaultEndOfDayStrategy implements EndOfDayStrategy {

    private static final int QUALITY_TO_SUBTRACT_FOR_NON_EXPIRED_ITEMS = 1;
    private static final int QUALITY_TO_SUBTRACT_FOR_EXPIRED_ITEMS = 2;

    @Override
    public void endOfDayUpdate(ItemProxy item) {
        updateQuality(item);
        updateAmountOfDaysUntilExpiration(item);
    }

    void updateQuality(ItemProxy item) {
        if(item.isExpired()) {
            item.decreaseQuality(qualityToDecreaseForExpiredItem());
        } else {
            item.decreaseQuality(qualityToDecreaseForNonExpiredItems());
        }
    }

    private void updateAmountOfDaysUntilExpiration(ItemProxy item) {
        item.decreaseAmountOfDaysUntilExpiration();
    }

    int qualityToDecreaseForExpiredItem() {
        return QUALITY_TO_SUBTRACT_FOR_EXPIRED_ITEMS;
    }

    int qualityToDecreaseForNonExpiredItems() {
        return QUALITY_TO_SUBTRACT_FOR_NON_EXPIRED_ITEMS;
    }
}