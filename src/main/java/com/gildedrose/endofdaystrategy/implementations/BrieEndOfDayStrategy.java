package com.gildedrose.endofdaystrategy.implementations;

import com.gildedrose.item.ItemProxy;

public class BrieEndOfDayStrategy extends DefaultEndOfDayStrategy {

    private static final int QUALITY_TO_APPEND_FOR_NON_EXPIRED_BRIE_ITEMS = 1;
    private static final int QUALITY_TO_APPEND_FOR_EXPIRED_BRIE_ITEMS = 2;

    @Override
    void updateQuality(ItemProxy item) {
        if (item.isExpired()) {
            item.increaseQuality(QUALITY_TO_APPEND_FOR_EXPIRED_BRIE_ITEMS);
        } else {
            item.increaseQuality(QUALITY_TO_APPEND_FOR_NON_EXPIRED_BRIE_ITEMS);
        }
    }
}