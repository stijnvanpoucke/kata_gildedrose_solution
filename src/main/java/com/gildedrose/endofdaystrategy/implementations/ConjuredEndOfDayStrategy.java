package com.gildedrose.endofdaystrategy.implementations;


public class ConjuredEndOfDayStrategy extends DefaultEndOfDayStrategy {

    @Override
    int qualityToDecreaseForExpiredItem() {
        return super.qualityToDecreaseForExpiredItem() * 2;
    }

    @Override
    int qualityToDecreaseForNonExpiredItems() {
        return super.qualityToDecreaseForNonExpiredItems() * 2;
    }
}
