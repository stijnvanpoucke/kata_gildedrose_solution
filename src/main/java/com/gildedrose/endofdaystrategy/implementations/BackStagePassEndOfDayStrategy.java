package com.gildedrose.endofdaystrategy.implementations;

import com.gildedrose.item.ItemProxy;

public class BackStagePassEndOfDayStrategy extends DefaultEndOfDayStrategy {

    private static final int QUALITY_TO_APPEND_ABOVE_TEN = 1;
    private static final int QUALITY_TO_APPEND_BETWEEN_TEN_AND_FIVE = 2;
    private static final int QUALITY_TO_APPEND_FIVE_AND_BELOW = 3;

    @Override
    void updateQuality(ItemProxy item) {
        if (item.isExpired()) {
            item.setQualityToZero();
        } else if (item.expirationBetween5and10days()) {
            item.increaseQuality(QUALITY_TO_APPEND_BETWEEN_TEN_AND_FIVE);
        } else if (item.expirationInLessThen5Days()) {
            item.increaseQuality(QUALITY_TO_APPEND_FIVE_AND_BELOW);
        } else {
            item.increaseQuality(QUALITY_TO_APPEND_ABOVE_TEN);
        }
    }
}